/*
 * Copyright © 2022 Konstantin Seurer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <filesystem>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <vector>

#include "bvh.h"

namespace fs = std::filesystem;

static void ValidateFloat(float f)
{
	if (isnanf(f))
	{
		if (isnanf(f) < 0)
		{
			std::cerr << "ERROR: -nan" << std::endl;
		}
		return;
	}

	if (!isnormal(f) && f != 0.0)
	{
		std::cerr << "WARNING: denormal/infinite float " << f << std::endl;
		return;
	}

	if (f > 100000.0)
	{
		std::cerr << "WARNING: float > 100000.0: " << f << std::endl;
		return;
	}
}

static void ValidateNode(std::shared_ptr<Node> node, const std::vector<std::shared_ptr<BVH>>& bvhs);

static void ValidateInternalNode(std::shared_ptr<InternalNode> node,
                                 const std::vector<std::shared_ptr<BVH>>& bvhs)
{
	for (uint32_t i = 0; i < 4; i++)
	{
		if (node->children[i])
		{
			ValidateNode(node->children[i], bvhs);

			for (uint32_t j = 0; j < 6; j++)
			{
				ValidateFloat(((float*)(void*)&node->internal.coords[i])[j]);
			}
		}
		else
		{
			for (uint32_t j = 0; j < 6; j++)
			{
				float f = ((float*)(void*)&node->internal.coords[i])[j];

				if (isnanf(f) == 0)
				{
					std::cerr << "ERROR: non nan bound with null child: " << f << std::endl;
				}
			}
		}
	}
}

static void ValidateInstanceNode(std::shared_ptr<Node> node,
                                 const std::vector<std::shared_ptr<BVH>>& bvhs)
{
	uint64_t instance_va = node->instance.base_ptr & (~63);
	std::shared_ptr<BVH> instance;
	for (auto bvh : bvhs)
	{
		if (bvh->va == instance_va)
		{
			instance = bvh;
		}
	}

	if (!instance)
	{
		std::cerr << "ERROR: Accel struct with va=" << std::hex << instance_va
				  << " (base_ptr=" << node->instance.base_ptr << ") does not exist" << std::endl;
	}
	else if (instance->header.instance_count)
	{
		std::cerr << "ERROR: Instanced BVH has instances" << std::endl;
	}

	for (uint32_t i = 0; i < 6; i++)
	{
		ValidateFloat(((float*)(void*)node->instance.aabb)[i]);
	}

	for (uint32_t i = 0; i < 12; i++)
	{
		ValidateFloat(node->instance.wto_matrix[i]);
	}

	for (uint32_t i = 0; i < 9; i++)
	{
		ValidateFloat(node->instance.otw_matrix[i]);
	}

	if (!(node->instance.custom_instance_and_mask & 0xFF000000))
	{
		std::cerr << "INFO: Invisible instance (mask=0)" << std::endl;
	}
}

static void ValidateTriangleNode(std::shared_ptr<Node> node)
{
	for (uint32_t i = 0; i < 9; i++)
	{
		ValidateFloat(((float*)(void*)node->triangle.coords)[i]);
	}
}

static void ValidateAABBNode(std::shared_ptr<Node> node)
{
	for (uint32_t i = 0; i < 6; i++)
	{
		ValidateFloat(((float*)(void*)node->aabb.aabb)[i]);
	}
}

static void ValidateNode(std::shared_ptr<Node> node, const std::vector<std::shared_ptr<BVH>>& bvhs)
{
	switch (node->type)
	{
	case radv_bvh_node_triangle: {
		ValidateTriangleNode(node);
		break;
	}
	case radv_bvh_node_internal: {
		ValidateInternalNode(std::dynamic_pointer_cast<InternalNode>(node), bvhs);
		break;
	}
	case radv_bvh_node_instance: {
		ValidateInstanceNode(node, bvhs);
		break;
	}
	case radv_bvh_node_aabb: {
		ValidateAABBNode(node);
		break;
	}
	default:
		break;
	}
}

static void ValidateBVH(std::shared_ptr<BVH> bvh, const std::vector<std::shared_ptr<BVH>>& bvhs)
{
	for (uint32_t i = 0; i < 6; i++)
	{
		ValidateFloat(((float*)(void*)bvh->header.aabb)[i]);
	}

	if (bvh->va & 63)
	{
		std::cerr << "ERROR: BVH is not 64 byte aligned" << std::endl;
	}

	if (bvh->header.compacted_size == 0)
	{
		std::cerr << "ERROR: compacted_size=0" << std::endl;
	}

	if (bvh->header.size == 0)
	{
		std::cerr << "ERROR: size=0" << std::endl;
	}

	if (bvh->header.serialization_size == 0)
	{
		std::cerr << "ERROR: serialization_size=0" << std::endl;
	}

	if (bvh->header.serialization_size < bvh->header.size)
	{
		std::cerr << "ERROR: serialization_size<size (serialization_size="
				  << bvh->header.serialization_size << ", size=" << bvh->header.size << ")"
				  << std::endl;
	}

	if (bvh->root)
	{
		if (bvh->root->type != radv_bvh_node_internal)
		{
			std::cerr << "INFO: non internal root node of type " << (uint32_t)bvh->root->type
					  << std::endl;
		}

		ValidateNode(bvh->root, bvhs);
	}
}

int main()
{
	std::vector<std::shared_ptr<BVH>> bvhs;

	fs::path cwd = fs::current_path();
	for (const auto& entry : fs::directory_iterator(cwd))
	{
		std::string filename = entry.path().filename().string();
		if (filename.find(".bvh") == std::string::npos)
		{
			continue;
		}

		std::cout << "Loading BVH '" << entry.path() << "'..." << std::endl;

		std::ifstream input(entry.path(), std::ios::binary);
		std::vector<uint8_t> buffer(std::istreambuf_iterator<char>(input), {});

		auto bvh = std::make_shared<BVH>(buffer.data(), buffer.size());
		bvh->va = strtoull(filename.substr(0, 16).c_str(), nullptr, 16);
		bvhs.push_back(bvh);
	}

	for (auto bvh : bvhs)
	{
		ValidateBVH(bvh, bvhs);
	}

	return 0;
}
