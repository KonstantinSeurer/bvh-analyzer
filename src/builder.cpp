/*
 * Copyright © 2022 Konstantin Seurer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "../tinygltf/tiny_gltf.h"

#include "vulkan_utils.h"

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

static VulkanContext context{};

static PFN_vkCreateAccelerationStructureKHR CreateAccelerationStructureKHR;
static PFN_vkDestroyAccelerationStructureKHR DestroyAccelerationStructureKHR;
static PFN_vkGetAccelerationStructureBuildSizesKHR GetAccelerationStructureBuildSizesKHR;
static PFN_vkBuildAccelerationStructuresKHR BuildAccelerationStructuresKHR;

struct VulkanAccelerationStructure
{
	VkAccelerationStructureKHR accelerationStructure;

	VulkanBuffer buffer;
};

static VulkanAccelerationStructure CreateAccelerationStructure(VkAccelerationStructureTypeKHR type,
                                                               uint32_t size)
{
	VulkanAccelerationStructure result;

	result.buffer = CreateBuffer(context, size);

	VkAccelerationStructureCreateInfoKHR info = {
		.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR,
		.pNext = nullptr,
		.createFlags = 0,
		.buffer = result.buffer.buffer,
		.offset = 0,
		.size = size,
		.type = type,
		.deviceAddress = 0,
	};

	VK_ASSERT(CreateAccelerationStructureKHR(context.device, &info, nullptr,
	                                         &result.accelerationStructure));

	return result;
}

static void DestroyAccelerationStructure(VulkanAccelerationStructure as)
{
	DestroyAccelerationStructureKHR(context.device, as.accelerationStructure, nullptr);
	DestroyBuffer(context, as.buffer);
}

static void DumpAccelerationStructure(VulkanAccelerationStructure as)
{
	void* data;
	VK_ASSERT(vkMapMemory(context.device, as.buffer.memory, 0, as.buffer.size, 0, &data));

	// FILE* file = fopen();
}

using namespace tinygltf;

static void LoadAndDumpScene(const Model& model, const Scene& scene)
{
	std::vector<VkAccelerationStructureInstanceKHR> instances;
	std::vector<VulkanAccelerationStructure> blas;

	for (int nodeID : scene.nodes)
	{
		const auto& node = model.nodes[nodeID];
		const auto& mesh = model.meshes[node.mesh];

		VkTransformMatrixKHR transformMatrix = {
			1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		};
		if (!node.matrix.empty())
		{
			float* transformMatrixDst = (float*)(void*)&transformMatrix;
			for (uint32_t i = 0; i < 12; i++)
			{
				transformMatrixDst[i] = node.matrix[i];
			}
		}

		for (const auto primitive : mesh.primitives)
		{
			const auto& posAccessor = model.accessors[primitive.attributes.at("POSITION")];
			assert(posAccessor.type == TINYGLTF_TYPE_VEC3);
			assert(posAccessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT);

			const auto& posView = model.bufferViews[posAccessor.bufferView];

			const auto& posBuffer = model.buffers[posView.buffer];
			const uint8_t* posData = posBuffer.data.data();
			uint32_t posStride = std::max(posView.byteStride, (size_t)12);

			std::vector<float> positions;
			for (uint32_t i = 0; i < posAccessor.count; i++)
			{
				uint32_t offset = i * posStride;
				positions.push_back(*(float*)(void*)(posData + offset));
				positions.push_back(*(float*)(void*)(posData + offset + 4));
				positions.push_back(*(float*)(void*)(posData + offset + 8));
			}

			const auto& indexAccessor = model.accessors[primitive.indices];
			assert(indexAccessor.type == TINYGLTF_TYPE_SCALAR);

			const auto& indexView = model.bufferViews[indexAccessor.bufferView];

			const auto& indexBuffer = model.buffers[indexView.buffer];
			const uint8_t* indexData = indexBuffer.data.data();

			size_t indexComponentSize = 0;
			switch (indexAccessor.componentType)
			{
			case TINYGLTF_COMPONENT_TYPE_BYTE:
			case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
				indexComponentSize = 1;
				break;
			case TINYGLTF_COMPONENT_TYPE_SHORT:
			case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
				indexComponentSize = 2;
				break;
			case TINYGLTF_COMPONENT_TYPE_INT:
			case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
				indexComponentSize = 4;
				break;
			default:
				abort();
			}

			uint32_t indexStride = std::max(indexView.byteStride, indexComponentSize);

			std::vector<uint32_t> indices;
			for (uint32_t i = 0; i < indexAccessor.count; i++)
			{
				uint32_t offset = i * indexStride;
				switch (indexAccessor.componentType)
				{
				case TINYGLTF_COMPONENT_TYPE_BYTE:
					indices.push_back(*(int8_t*)(indexData + offset));
					break;
				case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
					indices.push_back(*(uint8_t*)(indexData + offset));
					break;
				case TINYGLTF_COMPONENT_TYPE_SHORT:
					indices.push_back(*(int16_t*)(indexData + offset));
					break;
				case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
					indices.push_back(*(uint16_t*)(indexData + offset));
					break;
				case TINYGLTF_COMPONENT_TYPE_INT:
					indices.push_back(*(int32_t*)(indexData + offset));
					break;
				case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
					indices.push_back(*(uint32_t*)(indexData + offset));
					break;
				default:
					abort();
				}
			}

			VkAccelerationStructureGeometryKHR geometry = {
				.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR,
				.pNext = nullptr,
				.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR,
				.flags = VK_GEOMETRY_OPAQUE_BIT_KHR,
			};

			VkTransformMatrixKHR identityMatrix = {
				1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			};

			VkAccelerationStructureGeometryTrianglesDataKHR triangles = {
				.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR,
				.pNext = nullptr,
				.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT,
				.vertexData = VkDeviceOrHostAddressConstKHR{.hostAddress = positions.data()},
				.vertexStride = 12,
				.maxVertex = (uint32_t)posAccessor.count,
				.indexType = VK_INDEX_TYPE_UINT32,
				.indexData = VkDeviceOrHostAddressConstKHR{.hostAddress = indices.data()},
				.transformData = VkDeviceOrHostAddressConstKHR{.hostAddress = &identityMatrix},
			};
			geometry.geometry.triangles = triangles;

			VkAccelerationStructureBuildGeometryInfoKHR buildGeometryInfo = {
				.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR,
				.pNext = nullptr,
				.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR,
				.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR,
				.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR,
				.geometryCount = 1,
				.pGeometries = &geometry,
			};

			uint32_t primitive_count = indices.size() / 3;

			VkAccelerationStructureBuildSizesInfoKHR buildSizesInfo = {
				.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR,
			};
			GetAccelerationStructureBuildSizesKHR(
				context.device, VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR, &buildGeometryInfo,
				&primitive_count, &buildSizesInfo);

			VulkanAccelerationStructure as =
				CreateAccelerationStructure(VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR,
			                                buildSizesInfo.accelerationStructureSize);

			void* scratchBuffer = malloc(buildSizesInfo.buildScratchSize);

			buildGeometryInfo.scratchData.hostAddress = scratchBuffer;
			buildGeometryInfo.dstAccelerationStructure = as.accelerationStructure;

			VkAccelerationStructureBuildRangeInfoKHR buildRangeInfo = {
				.primitiveCount = primitive_count,
				.primitiveOffset = 0,
				.firstVertex = 0,
				.transformOffset = 0,
			};
			const VkAccelerationStructureBuildRangeInfoKHR* pBuildRangeInfo = &buildRangeInfo;

			VK_ASSERT(BuildAccelerationStructuresKHR(context.device, VK_NULL_HANDLE, 1,
			                                         &buildGeometryInfo, &pBuildRangeInfo));

			free(scratchBuffer);

			VkAccelerationStructureInstanceKHR instance = {
				.transform = transformMatrix,
				.instanceCustomIndex = 0,
				.mask = 0xFF,
				.instanceShaderBindingTableRecordOffset = 0,
				.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR,
				.accelerationStructureReference = (uint64_t)as.accelerationStructure,
			};

			instances.push_back(instance);

			blas.push_back(as);
		}
	}

	VkDeviceOrHostAddressConstKHR instanceDataDeviceAddress = {
		.hostAddress = instances.data(),
	};

	VkAccelerationStructureGeometryKHR geometry = {
		.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR,
		.pNext = nullptr,
		.geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR,
		.flags = VK_GEOMETRY_OPAQUE_BIT_KHR,
	};

	geometry.geometry.instances.sType =
		VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
	geometry.geometry.instances.pNext = nullptr;
	geometry.geometry.instances.arrayOfPointers = VK_FALSE;
	geometry.geometry.instances.data = instanceDataDeviceAddress;

	VkAccelerationStructureBuildGeometryInfoKHR buildGeometryInfo = {
		.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR,
		.pNext = nullptr,
		.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR,
		.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR,
		.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR,
		.geometryCount = 1,
		.pGeometries = &geometry,
	};

	uint32_t primitive_count = 1;

	VkAccelerationStructureBuildSizesInfoKHR buildSizesInfo = {
		.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR,
	};
	GetAccelerationStructureBuildSizesKHR(context.device,
	                                      VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
	                                      &buildGeometryInfo, &primitive_count, &buildSizesInfo);

	VulkanAccelerationStructure as = CreateAccelerationStructure(
		VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR, buildSizesInfo.accelerationStructureSize);

	void* scratchBuffer = malloc(buildSizesInfo.buildScratchSize);

	buildGeometryInfo.scratchData.hostAddress = scratchBuffer;
	buildGeometryInfo.dstAccelerationStructure = as.accelerationStructure;

	VkAccelerationStructureBuildRangeInfoKHR buildRangeInfo = {
		.primitiveCount = 1,
		.primitiveOffset = 0,
		.firstVertex = 0,
		.transformOffset = 0,
	};
	const VkAccelerationStructureBuildRangeInfoKHR* pBuildRangeInfo = &buildRangeInfo;

	VK_ASSERT(BuildAccelerationStructuresKHR(context.device, VK_NULL_HANDLE, 1, &buildGeometryInfo,
	                                         &pBuildRangeInfo));

	DestroyAccelerationStructure(as);
	free(scratchBuffer);

	for (const auto& as : blas)
	{
		DestroyAccelerationStructure(as);
	}
}

static void LoadAndDumpFile(const char* filename)
{
	Model model;
	TinyGLTF loader;
	std::string error;
	std::string warning;

	bool ret = loader.LoadASCIIFromFile(&model, &error, &warning, filename);

	if (!warning.empty())
	{
		std::cerr << "GLTF WARNING: " << error << std::endl;
	}

	if (!error.empty())
	{
		std::cerr << "GLTF ERROR: " << error << std::endl;
	}

	if (!ret)
	{
		return;
	}

	for (const auto& scene : model.scenes)
	{
		LoadAndDumpScene(model, scene);
	}
}

int main(int argc, char** argv)
{
	context = CreateContext();

	CreateAccelerationStructureKHR = (PFN_vkCreateAccelerationStructureKHR)vkGetDeviceProcAddr(
		context.device, "vkCreateAccelerationStructureKHR");
	DestroyAccelerationStructureKHR = (PFN_vkDestroyAccelerationStructureKHR)vkGetDeviceProcAddr(
		context.device, "vkDestroyAccelerationStructureKHR");
	GetAccelerationStructureBuildSizesKHR =
		(PFN_vkGetAccelerationStructureBuildSizesKHR)vkGetDeviceProcAddr(
			context.device, "vkGetAccelerationStructureBuildSizesKHR");
	BuildAccelerationStructuresKHR = (PFN_vkBuildAccelerationStructuresKHR)vkGetDeviceProcAddr(
		context.device, "vkBuildAccelerationStructuresKHR");

	for (uint32_t argumentIndex = 1; argumentIndex < argc; argumentIndex++)
	{
		LoadAndDumpFile(argv[argumentIndex]);
	}

	DestroyContext(context);

	return 0;
}
