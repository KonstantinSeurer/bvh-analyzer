/*
 * Copyright © 2022 Konstantin Seurer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <assert.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include "nlohmann/json.hpp"

#include "bvh.h"

namespace fs = std::filesystem;

struct PerNodeStatistics
{
	float area = 0.0f;
	uint32_t count = 0;
};

struct Statistics
{
	PerNodeStatistics triangle;
	PerNodeStatistics internal;
	PerNodeStatistics instance;
	PerNodeStatistics aabb;
};

static void AnalyzeNode(std::shared_ptr<Node> node, Statistics& stats, float area);

static void AnalyzeInternalNode(std::shared_ptr<InternalNode> node, Statistics& stats)
{
	stats.internal.count++;

	for (uint32_t i = 0; i < 4; i++)
	{
		if (node->children[i])
		{
			float width = node->internal.coords[i][1][0] - node->internal.coords[i][0][0];
			float height = node->internal.coords[i][1][1] - node->internal.coords[i][0][1];
			float length = node->internal.coords[i][1][2] - node->internal.coords[i][0][2];
			float area = (width * height + width * length + height * length) * 2.0f;

			AnalyzeNode(node->children[i], stats, area);
		}
		else
		{
		}
	}
}

static void AnalyzeInstanceNode(std::shared_ptr<Node> node, Statistics& stats)
{
	stats.instance.count++;
}

static void AnalyzeTriangleNode(std::shared_ptr<Node> node, Statistics& stats)
{
	stats.triangle.count++;
}

static void AnalyzeAABBNode(std::shared_ptr<Node> node, Statistics& stats)
{
	stats.aabb.count++;
}

static void AnalyzeNode(std::shared_ptr<Node> node, Statistics& stats, float area)
{
	switch (node->type)
	{
	case radv_bvh_node_triangle: {
		stats.triangle.area += area;
		AnalyzeTriangleNode(node, stats);
		break;
	}
	case radv_bvh_node_internal: {
		stats.internal.area += area;
		AnalyzeInternalNode(std::dynamic_pointer_cast<InternalNode>(node), stats);
		break;
	}
	case radv_bvh_node_instance: {
		stats.instance.area += area;
		AnalyzeInstanceNode(node, stats);
		break;
	}
	case radv_bvh_node_aabb: {
		stats.aabb.area += area;
		AnalyzeAABBNode(node, stats);
		break;
	}
	default:
		break;
	}
}

static void AnalyzeBVH(std::shared_ptr<BVH> bvh, Statistics& stats)
{
	if (bvh->root)
	{
		AnalyzeNode(bvh->root, stats, 0.0f);
	}
}

static void PrintHelp()
{
}

static nlohmann::json SerializePerNodeStatistics(const PerNodeStatistics& stats)
{
	nlohmann::json json;

	json["area"] = stats.area;
	json["count"] = stats.count;

	return json;
}

static void DumpStatistics(const Statistics& stats, const std::string& file)
{
	nlohmann::json json;

	json["triangle"] = SerializePerNodeStatistics(stats.triangle);
	json["internal"] = SerializePerNodeStatistics(stats.internal);
	json["instance"] = SerializePerNodeStatistics(stats.instance);
	json["aabb"] = SerializePerNodeStatistics(stats.aabb);

	std::ofstream output(file);
	output << std::setw(4) << json << std::endl;
}

int main(int argc, char** argv)
{
	if (argc < 2 || strcmp(argv[1], "help") == 0)
	{
		PrintHelp();
		return 0;
	}

	if (strcmp(argv[1], "analyze") == 0)
	{
		assert(argc == 4);

		std::cout << "Loading BVH '" << argv[2] << "'..." << std::endl;

		std::ifstream input(argv[2], std::ios::binary);
		std::vector<uint8_t> buffer(std::istreambuf_iterator<char>(input), {});

		auto bvh = std::make_shared<BVH>(buffer.data(), buffer.size());

		Statistics stats;
		AnalyzeBVH(bvh, stats);

		DumpStatistics(stats, argv[3]);

		return 0;
	}

	std::cout << "Invalid command '" << argv[1] << "'!" << std::endl;
	PrintHelp();
	return 0;
}
