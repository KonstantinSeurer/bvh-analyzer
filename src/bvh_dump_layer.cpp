
#include "vulkan_utils.h"

// Include vkroots after we included the Vulkan headers to ensure, that we always have the latest
// headers available.
#include "../vkroots/vkroots.h"

#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>

struct radv_accel_struct_header
{
	uint32_t root_node_offset;
	uint32_t reserved;
	float aabb[2][3];

	/* Everything after this gets updated/copied from the CPU. */
	uint64_t compacted_size;
	uint64_t serialization_size;
	uint32_t copy_dispatch_size[3];
	uint64_t instance_offset;
	uint64_t instance_count;
	uint64_t size;
};

namespace bvh_dump_layer
{

struct AccelStructData
{
	VkBuffer buffer;
	VkDeviceSize offset;
	VulkanBuffer readBack;
};

static std::unordered_map<VkAccelerationStructureKHR, AccelStructData> accel_structs;

struct CmdBufferData
{
	const vkroots::VkDeviceDispatch* dispatch;
	std::vector<VkAccelerationStructureKHR> accel_struct_builds;
};

static std::unordered_map<VkCommandBuffer, CmdBufferData> cmd_buffers;

static std::unordered_map<VkQueue, VkDevice> queues;

static VulkanContext GetVulkanContext(const vkroots::VkDeviceDispatch* pDispatch, VkDevice device)
{
	const vkroots::VkInstanceDispatch* pInstanceDispatch =
		vkroots::tables::LookupInstanceDispatch(pDispatch->PhysicalDevice);

	VulkanContext context;
	context.EnumeratePhysicalDevices = pInstanceDispatch->EnumeratePhysicalDevices;
	context.EnumerateDeviceExtensionProperties =
		pInstanceDispatch->EnumerateDeviceExtensionProperties;
	context.GetPhysicalDeviceQueueFamilyProperties =
		pInstanceDispatch->GetPhysicalDeviceQueueFamilyProperties;
	context.GetPhysicalDeviceMemoryProperties =
		pInstanceDispatch->GetPhysicalDeviceMemoryProperties;
	context.CreateDevice = pInstanceDispatch->CreateDevice;
	context.CreateBuffer = pDispatch->CreateBuffer;
	context.GetBufferMemoryRequirements = pDispatch->GetBufferMemoryRequirements;
	context.AllocateMemory = pDispatch->AllocateMemory;
	context.BindBufferMemory = pDispatch->BindBufferMemory;
	context.DestroyBuffer = pDispatch->DestroyBuffer;
	context.FreeMemory = pDispatch->FreeMemory;
	context.GetBufferDeviceAddress = pDispatch->GetBufferDeviceAddress;

	context.physicalDevice = pDispatch->PhysicalDevice;
	context.device = device;
	SelectQueueFamily(context);

	return context;
}

class VkDeviceOverrides
{
public:
	static void GetDeviceQueue(const vkroots::VkDeviceDispatch* pDispatch, VkDevice device,
	                           uint32_t queueFamilyIndex, uint32_t queueIndex, VkQueue* pQueue)
	{
		pDispatch->GetDeviceQueue(device, queueFamilyIndex, queueIndex, pQueue);
		queues[*pQueue] = device;
	}

	static void GetDeviceQueue2(const vkroots::VkDeviceDispatch* pDispatch, VkDevice device,
	                            const VkDeviceQueueInfo2* pQueueInfo, VkQueue* pQueue)
	{
		pDispatch->GetDeviceQueue2(device, pQueueInfo, pQueue);
		queues[*pQueue] = device;
	}

	static VkResult CreateAccelerationStructureKHR(
		const vkroots::VkDeviceDispatch* pDispatch, VkDevice device,
		const VkAccelerationStructureCreateInfoKHR* pCreateInfo,
		const VkAllocationCallbacks* pAllocator, VkAccelerationStructureKHR* pAccelerationStructure)
	{
		VkResult result = pDispatch->CreateAccelerationStructureKHR(device, pCreateInfo, pAllocator,
		                                                            pAccelerationStructure);

		if (result != VK_SUCCESS)
		{
			return result;
		}

		VulkanContext context = GetVulkanContext(pDispatch, device);

		AccelStructData data;
		data.buffer = pCreateInfo->buffer;
		data.offset = pCreateInfo->offset;
		data.readBack = CreateBuffer(context, pCreateInfo->size);
		accel_structs[*pAccelerationStructure] = data;

		return VK_SUCCESS;
	}

	static void DestroyAccelerationStructureKHR(const vkroots::VkDeviceDispatch* pDispatch,
	                                            VkDevice device,
	                                            VkAccelerationStructureKHR accelerationStructure,
	                                            const VkAllocationCallbacks* pAllocator)
	{
		pDispatch->DestroyAccelerationStructureKHR(device, accelerationStructure, pAllocator);

		VulkanContext context = GetVulkanContext(pDispatch, device);

		AccelStructData data = accel_structs.at(accelerationStructure);
		DestroyBuffer(context, data.readBack);
	}

	static VkResult BeginCommandBuffer(const vkroots::VkDeviceDispatch* pDispatch,
	                                   VkCommandBuffer commandBuffer,
	                                   const VkCommandBufferBeginInfo* pBeginInfo)
	{
		CmdBufferData& cmd_data = cmd_buffers.at(commandBuffer);
		pDispatch = cmd_data.dispatch;
		cmd_data.accel_struct_builds.clear();

		return pDispatch->BeginCommandBuffer(commandBuffer, pBeginInfo);
	}

	static void CmdBuildAccelerationStructuresKHR(
		const vkroots::VkDeviceDispatch* pDispatch, VkCommandBuffer commandBuffer,
		uint32_t infoCount, const VkAccelerationStructureBuildGeometryInfoKHR* pInfos,
		const VkAccelerationStructureBuildRangeInfoKHR* const* ppBuildRangeInfos)
	{
		CmdBufferData& cmd_data = cmd_buffers.at(commandBuffer);
		pDispatch = cmd_data.dispatch;

		pDispatch->CmdBuildAccelerationStructuresKHR(commandBuffer, infoCount, pInfos,
		                                             ppBuildRangeInfos);

		VkMemoryBarrier barrier = {
			.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER,
			.pNext = NULL,
			.srcAccessMask = VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR,
			.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
		};

		pDispatch->CmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
		                              VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 1, &barrier, 0,
		                              nullptr, 0, nullptr);

		for (uint32_t i = 0; i < infoCount; i++)
		{
			AccelStructData data = accel_structs.at(pInfos[i].dstAccelerationStructure);

			VkBufferCopy region = {
				.srcOffset = data.offset,
				.dstOffset = 0,
				.size = data.readBack.size,
			};

			pDispatch->CmdCopyBuffer(commandBuffer, data.buffer, data.readBack.buffer, 1, &region);

			cmd_data.accel_struct_builds.push_back(pInfos[i].dstAccelerationStructure);
		}
	}

	static VkResult AllocateCommandBuffers(const vkroots::VkDeviceDispatch* pDispatch,
	                                       VkDevice device,
	                                       const VkCommandBufferAllocateInfo* pAllocateInfo,
	                                       VkCommandBuffer* pCommandBuffers)
	{
		VkResult result = pDispatch->AllocateCommandBuffers(device, pAllocateInfo, pCommandBuffers);

		if (result != VK_SUCCESS)
		{
			return result;
		}

		for (uint32_t i = 0; i < pAllocateInfo->commandBufferCount; i++)
		{
			CmdBufferData data;
			data.dispatch = pDispatch;
			cmd_buffers[pCommandBuffers[i]] = data;
		}

		return VK_SUCCESS;
	}

	static void FreeCommandBuffers(const vkroots::VkDeviceDispatch* pDispatch, VkDevice device,
	                               VkCommandPool commandPool, uint32_t commandBufferCount,
	                               const VkCommandBuffer* pCommandBuffers)
	{
		pDispatch->FreeCommandBuffers(device, commandPool, commandBufferCount, pCommandBuffers);

		for (uint32_t i = 0; i < commandBufferCount; i++)
		{
			cmd_buffers.erase(pCommandBuffers[i]);
		}
	}

	static VkResult DumpAccelStruct(const vkroots::VkDeviceDispatch* pDispatch, VkDevice device,
	                                VkAccelerationStructureKHR accelStruct)
	{
		const AccelStructData& data = accel_structs.at(accelStruct);

		void* dump;
		VkResult result =
			pDispatch->MapMemory(device, data.readBack.memory, 0, data.readBack.size, 0, &dump);
		if (result != VK_SUCCESS)
		{
			return result;
		}

		const char* dumpPath = std::getenv("ACCELERATION_STRUCTURE_DUMP_PATH");
		if (!dumpPath)
		{
			dumpPath = std::filesystem::current_path().c_str();
		}

		VkAccelerationStructureDeviceAddressInfoKHR devAddrInfo = {
			.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR,
			.pNext = nullptr,
			.accelerationStructure = accelStruct,
		};

		VkDeviceAddress address =
			pDispatch->GetAccelerationStructureDeviceAddressKHR(device, &devAddrInfo);

		char* path = (char*)malloc(strlen(dumpPath) + 1 + 16 + 4 + 1);
		sprintf(path, "%s/%lx.bvh", dumpPath, address);

		FILE* file = fopen(path, "w");
		fwrite(dump, 1, data.readBack.size, file);
		fclose(file);

		free(path);

		pDispatch->UnmapMemory(device, data.readBack.memory);

		return VK_SUCCESS;
	}

	static VkResult DumpAccelStructs(const vkroots::VkDeviceDispatch* pDispatch, VkQueue queue,
	                                 uint32_t commandBufferCount,
	                                 const VkCommandBuffer* pCommandBuffers)
	{
		VkResult result = pDispatch->QueueWaitIdle(queue);
		if (result != VK_SUCCESS)
		{
			return result;
		}

		VkDevice device = queues.at(queue);

		for (uint32_t i = 0; i < commandBufferCount; i++)
		{
			const CmdBufferData& data = cmd_buffers.at(pCommandBuffers[i]);
			for (uint32_t accelStructIndex = 0; accelStructIndex < data.accel_struct_builds.size();
			     accelStructIndex++)
			{
				result =
					DumpAccelStruct(pDispatch, device, data.accel_struct_builds[accelStructIndex]);
				if (result != VK_SUCCESS)
				{
					return result;
				}
			}
		}

		return VK_SUCCESS;
	}

	static VkResult QueueSubmit(const vkroots::VkDeviceDispatch* pDispatch, VkQueue queue,
	                            uint32_t submitCount, const VkSubmitInfo* pSubmits, VkFence fence)
	{
		VkResult result = pDispatch->QueueSubmit(queue, submitCount, pSubmits, fence);
		if (result != VK_SUCCESS)
		{
			return result;
		}

		for (uint32_t i = 0; i < submitCount; i++)
		{
			result = DumpAccelStructs(pDispatch, queue, pSubmits[i].commandBufferCount,
			                          pSubmits[i].pCommandBuffers);
			if (result != VK_SUCCESS)
			{
				return result;
			}
		}

		return VK_SUCCESS;
	}

	static VkResult QueueSubmit2(const vkroots::VkDeviceDispatch* pDispatch, VkQueue queue,
	                             uint32_t submitCount, const VkSubmitInfo2* pSubmits, VkFence fence)
	{
		VkResult result = pDispatch->QueueSubmit2(queue, submitCount, pSubmits, fence);
		if (result != VK_SUCCESS)
		{
			return result;
		}

		for (uint32_t i = 0; i < submitCount; i++)
		{
			std::vector<VkCommandBuffer> commandBuffers(pSubmits[i].commandBufferInfoCount);
			for (uint32_t cmdIndex = 0; cmdIndex < pSubmits[i].commandBufferInfoCount; cmdIndex++)
			{
				commandBuffers[cmdIndex] = pSubmits[i].pCommandBufferInfos[cmdIndex].commandBuffer;
			}

			result =
				DumpAccelStructs(pDispatch, queue, commandBuffers.size(), commandBuffers.data());
			if (result != VK_SUCCESS)
			{
				return result;
			}
		}

		return VK_SUCCESS;
	}

	static VkResult QueueSubmit2KHR(const vkroots::VkDeviceDispatch* pDispatch, VkQueue queue,
	                                uint32_t submitCount, const VkSubmitInfo2KHR* pSubmits,
	                                VkFence fence)
	{
		VkResult result = pDispatch->QueueSubmit2(queue, submitCount, pSubmits, fence);
		if (result != VK_SUCCESS)
		{
			return result;
		}

		for (uint32_t i = 0; i < submitCount; i++)
		{
			std::vector<VkCommandBuffer> commandBuffers(pSubmits[i].commandBufferInfoCount);
			for (uint32_t cmdIndex = 0; cmdIndex < pSubmits[i].commandBufferInfoCount; cmdIndex++)
			{
				commandBuffers[cmdIndex] = pSubmits[i].pCommandBufferInfos[cmdIndex].commandBuffer;
			}

			result =
				DumpAccelStructs(pDispatch, queue, commandBuffers.size(), commandBuffers.data());
			if (result != VK_SUCCESS)
			{
				return result;
			}
		}

		return VK_SUCCESS;
	}
};

} // namespace bvh_dump_layer

VKROOTS_DEFINE_LAYER_INTERFACES(vkroots::NoOverrides, vkroots::NoOverrides,
                                bvh_dump_layer::VkDeviceOverrides);
