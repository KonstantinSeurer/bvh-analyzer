
#include <cmath>
#include <iostream>

#include "bvh.h"

#define MAX_BVH_DEPTH 12

static std::shared_ptr<Node> CreateNode(const uint8_t* data, size_t size, uint32_t id,
                                        uint32_t depth)
{
	if (depth > MAX_BVH_DEPTH)
	{
		std::cerr << "ERROR: BVH is to deep" << std::endl;
		return nullptr;
	}

	radv_bvh_node_type type = (radv_bvh_node_type)(id & 0b111);
	uint32_t offset = (id & (~0b111)) << 3;

	if (offset >= size)
	{
		std::cerr << "ERROR: Out of bounds offset " << offset << " (id=" << id << ", size=" << size
				  << ")" << std::endl;
		return NULL;
	}

	switch (type)
	{
	case radv_bvh_node_triangle: {
		std::shared_ptr<Node> result = std::make_shared<Node>(radv_bvh_node_triangle);
		result->triangle = *(radv_bvh_triangle_node*)(data + offset);
		return result;
	}
	case radv_bvh_node_internal: {
		std::shared_ptr<InternalNode> result = std::make_shared<InternalNode>();
		result->internal = *(radv_bvh_box32_node*)(data + offset);

		for (uint32_t i = 0; i < 4; i++)
		{
			if (isnanf(result->internal.coords[i][0][0]))
			{
				result->children[i] = nullptr;
			}
			else
			{
				result->children[i] =
					CreateNode(data, size, result->internal.children[i], depth + 1);
			}
		}

		return result;
	}
	case radv_bvh_node_instance: {
		std::shared_ptr<Node> result = std::make_shared<Node>(radv_bvh_node_instance);
		result->instance = *(radv_bvh_instance_node*)(data + offset);

		return result;
	}
	case radv_bvh_node_aabb: {
		std::shared_ptr<Node> result = std::make_shared<Node>(radv_bvh_node_aabb);
		result->aabb = *(radv_bvh_aabb_node*)(data + offset);
		return result;
	}
	default:
		break;
	}

	std::cerr << "ERROR: Invalid node type " << (uint32_t)type << std::endl;
	return NULL;
}

BVH::BVH(const uint8_t* data, size_t size)
{
	header = *(radv_accel_struct_header*)data;

	root = CreateNode(data, size, header.root_node_offset, 0);
	if (!root)
	{
		return;
	}

	if (root->type != radv_bvh_node_internal)
	{
		std::cerr << "WARNING: Unexpected node type for root " << (uint32_t)root->type << std::endl;
	}
}
