/*
 * Copyright © 2022 Konstantin Seurer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <assert.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <unordered_set>
#include <vector>

#include "GLFW/glfw3.h"

#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"

#include "glm/glm.hpp"

#include "bvh.h"

static GLFWwindow* OpenWindow()
{
	if (!glfwInit())
		return nullptr;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	GLFWwindow* window = glfwCreateWindow(1280, 700, "bvh visualizer", nullptr, nullptr);
	if (!window)
		return NULL;

	glfwMakeContextCurrent(window);

	return window;
}

namespace fs = std::filesystem;

struct AABB
{
	glm::vec3 min;
	glm::vec3 max;
};

struct AABBHash
{
	size_t operator()(const AABB& aabb) const
	{
		uint32_t hash = std::hash<float>()(aabb.min.x);
		hash ^= std::hash<float>()(aabb.min.y);
		hash ^= std::hash<float>()(aabb.min.z);

		hash ^= std::hash<float>()(aabb.max.x);
		hash ^= std::hash<float>()(aabb.max.y);
		hash ^= std::hash<float>()(aabb.max.z);

		return hash;
	}
};

static inline bool operator==(const AABB& a, const AABB& b)
{
	return a.min == b.min && a.max == b.max;
}

struct Triangle
{
	glm::vec3 coords[3];
};

struct Mesh
{
	std::vector<AABB> aabbs;
	std::vector<Triangle> triangles;
	float color[3];
};

class Camera
{
private:
	float position[3];
	float rotation[2];

public:
	Camera()
	{
		position[0] = 0.0;
		position[1] = 0.0;
		position[2] = -10.0;

		rotation[0] = 0.0;
		rotation[1] = 0.0;
	}

	void Update(GLFWwindow* window, float delta, float translation_speed, float rotation_speed)
	{
		float dt = translation_speed * delta;
		float dr = rotation_speed * delta;

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		{
			position[0] += -dt * sin(rotation[1]);
			position[2] += dt * cos(rotation[1]);
		}

		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		{
			position[0] -= -dt * sin(rotation[1]);
			position[2] -= dt * cos(rotation[1]);
		}

		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		{
			position[0] += -dt * sin(rotation[1] - M_PI_2);
			position[2] += dt * cos(rotation[1] - M_PI_2);
		}

		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		{
			position[0] += -dt * sin(rotation[1] + M_PI_2);
			position[2] += dt * cos(rotation[1] + M_PI_2);
		}

		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
		{
			position[1] -= dt;
		}

		if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		{
			position[1] += dt;
		}

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		{
			rotation[0] -= dr;
		}

		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		{
			rotation[0] += dr;
		}

		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		{
			rotation[1] -= dr;
		}

		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		{
			rotation[1] += dr;
		}
	}

	void SetModelViewMatrix() const
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glRotatef(rotation[0] / M_PI * 180.0, 1.0, 0.0, 0.0);
		glRotatef(rotation[1] / M_PI * 180.0, 0.0, 1.0, 0.0);
		glTranslatef(position[0], position[1], position[2]);
	}
};

class Time
{
public:
	uint64_t value;

public:
	Time()
	{
		std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
		auto duration = now.time_since_epoch();
		auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(duration);
		value = nanoseconds.count();
	}

	Time(uint64_t value) : value(value)
	{
	}

	float seconds() const
	{
		return value / 1000000000.0;
	}

	Time operator-(Time other) const
	{
		return Time(value - other.value);
	}
};

static void CreateNodeMesh(std::shared_ptr<Node> node, std::unordered_set<AABB, AABBHash>& aabbs,
                           std::vector<Triangle>& triangles);

static void CreateInternalNodeMesh(std::shared_ptr<InternalNode> node,
                                   std::unordered_set<AABB, AABBHash>& aabbs,
                                   std::vector<Triangle>& triangles)
{
	for (uint32_t i = 0; i < 4; i++)
	{
		if (!node->children[i])
		{
			continue;
		}

		AABB aabb;

		aabb.min.x = node->internal.coords[i][0][0];
		aabb.min.y = node->internal.coords[i][0][1];
		aabb.min.z = node->internal.coords[i][0][2];

		aabb.max.x = node->internal.coords[i][1][0];
		aabb.max.y = node->internal.coords[i][1][1];
		aabb.max.z = node->internal.coords[i][1][2];

		aabbs.insert(aabb);

		CreateNodeMesh(node->children[i], aabbs, triangles);
	}
}

static void CreateTriangleNodeMesh(std::shared_ptr<Node> node,
                                   std::unordered_set<AABB, AABBHash>& aabbs,
                                   std::vector<Triangle>& triangles)
{
	Triangle triangle;

	for (uint32_t coord = 0; coord < 3; coord++)
	{
		triangle.coords[coord].x = node->triangle.coords[coord][0];
		triangle.coords[coord].y = node->triangle.coords[coord][1];
		triangle.coords[coord].z = node->triangle.coords[coord][2];
	}

	triangles.push_back(triangle);
}

static void CreateNodeMesh(std::shared_ptr<Node> node, std::unordered_set<AABB, AABBHash>& aabbs,
                           std::vector<Triangle>& triangles)
{
	if (node->type == radv_bvh_node_internal)
	{
		CreateInternalNodeMesh(std::dynamic_pointer_cast<InternalNode>(node), aabbs, triangles);
	}
	else if (node->type == radv_bvh_node_triangle)
	{
		CreateTriangleNodeMesh(node, aabbs, triangles);
	}
}

static void CreateBVHMesh(std::shared_ptr<BVH> bvh, std::unordered_set<AABB, AABBHash>& aabbs,
                          std::vector<Triangle>& triangles)
{
	AABB aabb;

	aabb.min.x = bvh->header.aabb[0][0];
	aabb.min.y = bvh->header.aabb[0][1];
	aabb.min.z = bvh->header.aabb[0][2];

	aabb.max.x = bvh->header.aabb[1][0];
	aabb.max.y = bvh->header.aabb[1][1];
	aabb.max.z = bvh->header.aabb[1][2];

	aabbs.insert(aabb);

	if (bvh->root)
	{
		CreateNodeMesh(bvh->root, aabbs, triangles);
	}
}

#define NO_SELECTION 0xFFFFFFFF

class Visualizer
{
public:
	GLFWwindow* window;

	std::vector<std::shared_ptr<BVH>> bvhs;
	std::vector<Mesh> bvh_meshes;

	Camera camera;

	Time lastFrame;

	bool zUp;
	bool drawAABBs;
	float translation_speed;
	float max_bvh_size;
	float rotation_speed;
	float background[3];
	float wireframeColor[3];

	uint32_t selectedTLAS = NO_SELECTION;

public:
	Visualizer()
	{
		zUp = false;
		drawAABBs = false;

		background[0] = 0.7f;
		background[1] = 0.7f;
		background[2] = 0.7f;

		wireframeColor[0] = 0.2f;
		wireframeColor[1] = 0.2f;
		wireframeColor[2] = 0.2f;

		rotation_speed = M_PI / 2.0;
	}

	bool Open()
	{
		window = OpenWindow();
		if (!window)
			return false;

		IMGUI_CHECKVERSION();
		ImGui::CreateContext();

		ImGui::StyleColorsDark();

		ImGui_ImplGlfw_InitForOpenGL(window, true);
		ImGui_ImplOpenGL3_Init();

		ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_DockingEnable;

		return true;
	}

	void LoadBVHs()
	{
		fs::path cwd = fs::current_path();
		for (const auto& entry : fs::directory_iterator(cwd))
		{
			if (entry.path().filename().string().find(".bvh") == std::string::npos)
			{
				continue;
			}

			std::cout << "Loading BVH '" << entry.path() << "'..." << std::endl;

			std::ifstream input(entry.path(), std::ios::binary);
			std::vector<uint8_t> buffer(std::istreambuf_iterator<char>(input), {});

			auto bvh = std::make_shared<BVH>(buffer.data(), buffer.size());
			bvh->va = strtoull(entry.path().filename().string().substr(0, 16).c_str(), nullptr, 16);

			if (bvh->header.instance_count && selectedTLAS == NO_SELECTION)
			{
				selectedTLAS = bvhs.size();
			}

			bvhs.push_back(bvh);
		}

		translation_speed = 0.0;
		for (auto bvh : bvhs)
		{
			float size = bvh->header.aabb[1][0] - bvh->header.aabb[0][0];
			if (isnan(size))
			{
				continue;
			}

			translation_speed = std::max(translation_speed, size);
		}
		max_bvh_size = translation_speed;

		bvh_meshes.reserve(bvhs.size());

		for (auto bvh : bvhs)
		{
			std::unordered_set<AABB, AABBHash> aabb_set;
			std::vector<Triangle> triangles;
			CreateBVHMesh(bvh, aabb_set, triangles);

			std::vector<AABB> aabbs;
			aabbs.reserve(aabb_set.size());

			for (const AABB& aabb : aabb_set)
			{
				aabbs.push_back(aabb);
			}

			Mesh mesh = {
				.aabbs = aabbs,
				.triangles = triangles,
			};

			mesh.color[0] = (float)rand() / (float)RAND_MAX;
			mesh.color[1] = (float)rand() / (float)RAND_MAX;
			mesh.color[2] = (float)rand() / (float)RAND_MAX;

			bvh_meshes.push_back(mesh);
		}
	}

	void Close()
	{
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();

		glfwDestroyWindow(window);
		glfwTerminate();
	}

	void DrawWireframeCube(const AABB& aabb)
	{
		float* bounds = (float*)&aabb;

		glColor3f(wireframeColor[0], wireframeColor[1], wireframeColor[2]);

		// vertical
		glVertex3f(bounds[0], bounds[1], bounds[2]);
		glVertex3f(bounds[0], bounds[4], bounds[2]);

		glVertex3f(bounds[3], bounds[1], bounds[2]);
		glVertex3f(bounds[3], bounds[4], bounds[2]);

		glVertex3f(bounds[0], bounds[1], bounds[5]);
		glVertex3f(bounds[0], bounds[4], bounds[5]);

		glVertex3f(bounds[3], bounds[1], bounds[5]);
		glVertex3f(bounds[3], bounds[4], bounds[5]);

		// horizontal
		for (uint32_t yindex = 0; yindex < 2; yindex++)
		{
			float y = bounds[1 + yindex * 3];

			glVertex3f(bounds[0], y, bounds[2]);
			glVertex3f(bounds[3], y, bounds[2]);

			glVertex3f(bounds[0], y, bounds[5]);
			glVertex3f(bounds[3], y, bounds[5]);

			glVertex3f(bounds[0], y, bounds[2]);
			glVertex3f(bounds[0], y, bounds[5]);

			glVertex3f(bounds[3], y, bounds[2]);
			glVertex3f(bounds[3], y, bounds[5]);
		}
	}

	void DrawInstances(std::shared_ptr<Node> node)
	{
		if (!node)
		{
			return;
		}

		if (node->type == radv_bvh_node_instance)
		{
			glPushMatrix();

			float matrix[16];
			for (uint32_t i = 0; i < 4; i++)
			{
				for (uint32_t j = 0; j < 4; j++)
				{
					float value;

					if (i == j && i == 3)
					{
						value = 1.0;
					}
					else if (i == 3 || j == 3)
					{
						value = 0.0;
					}
					else
					{
						value = node->instance.otw_matrix[i + j * 3];
					}

					matrix[i + j * 4] = value;
				}
			}
			glMultMatrixf(matrix);

			glTranslatef(node->instance.wto_matrix[3], node->instance.wto_matrix[7],
			             node->instance.wto_matrix[11]);

			uint64_t instance_va = node->instance.base_ptr & (~63);
			for (uint32_t i = 0; i < bvhs.size(); i++)
			{
				if (bvhs[i]->va != instance_va)
				{
					continue;
				}

				const auto& mesh = bvh_meshes[i];

				uint32_t seed = i;

				glBegin(GL_TRIANGLES);
				for (const auto& triangle : mesh.triangles)
				{
					glm::vec3 normal =
						glm::normalize(glm::cross(triangle.coords[1] - triangle.coords[0],
					                              triangle.coords[2] - triangle.coords[0]));
					float lighting = (-(zUp ? normal.z : normal.y) * 0.5 + 0.5) * 0.5 + 0.5;

					float r = 0.9f * mesh.color[0] + 0.1f * (float)rand_r(&seed) / (float)RAND_MAX;
					float g = 0.9f * mesh.color[1] + 0.1f * (float)rand_r(&seed) / (float)RAND_MAX;
					float b = 0.9f * mesh.color[2] + 0.1f * (float)rand_r(&seed) / (float)RAND_MAX;
					glColor3f(r * lighting, g * lighting, b * lighting);

					for (uint32_t coordIndex = 0; coordIndex < 3; coordIndex++)
					{
						const auto& coord = triangle.coords[coordIndex];
						glVertex3f(coord.x, coord.y, coord.z);
					}
				}
				glEnd();

				if (drawAABBs)
				{
					glBegin(GL_LINES);
					for (uint32_t aabbIndex = 0; aabbIndex < mesh.aabbs.size(); aabbIndex++)
					{
						DrawWireframeCube(mesh.aabbs[aabbIndex]);
					}
					glEnd();
				}

				break;
			}

			glPopMatrix();
		}
		else if (node->type == radv_bvh_node_internal)
		{
			auto internal = std::dynamic_pointer_cast<InternalNode>(node);

			for (uint32_t i = 0; i < 4; i++)
			{
				DrawInstances(internal->children[i]);
			}
		}
	}

	void DrawViewport(float aspect)
	{
		glClearColor(background[0], background[1], background[2], 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (selectedTLAS == NO_SELECTION)
			return;

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glFrustum(-aspect * 0.5, aspect * 0.5, -0.5, 0.5, 0.5, max_bvh_size * 2.0);

		Time currentTime;
		float delta = (currentTime - lastFrame).seconds();
		lastFrame = currentTime;

		camera.Update(window, delta, translation_speed, rotation_speed);
		camera.SetModelViewMatrix();

		auto bvh = bvhs[selectedTLAS];
		const auto& mesh = bvh_meshes[selectedTLAS];

		glPushMatrix();
		glTranslatef(-bvh->header.aabb[0][0], 0.0, -bvh->header.aabb[1][2]);

		if (zUp)
		{
			glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
		}

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		DrawInstances(bvh->root);

		if (drawAABBs)
		{
			glBegin(GL_LINES);
			for (uint32_t aabbIndex = 0; aabbIndex < mesh.aabbs.size(); aabbIndex++)
			{
				DrawWireframeCube(mesh.aabbs[aabbIndex]);
			}
			glEnd();
		}

		glPopMatrix();
	}

	bool UpdateAndRender()
	{
		glfwPollEvents();
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		int width, height;
		glfwGetWindowSize(window, &width, &height);
		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT);

		const ImGuiViewport* viewport = ImGui::GetMainViewport();
		ImGui::SetNextWindowPos(viewport->WorkPos);
		ImGui::SetNextWindowSize(viewport->WorkSize);
		ImGui::SetNextWindowViewport(viewport->ID);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGuiWindowFlags window_flags =
			ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse |
			ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove |
			ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

		ImGui::Begin("::empty", nullptr, window_flags);
		ImGui::PopStyleVar(3);

		ImGuiID dockspace_id = ImGui::GetID("dockspace");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f));

		ImGui::Begin("Properties");
		{
			ImGui::BeginTabBar("Properties#tab_bar");

			if (ImGui::BeginTabItem("Viewport"))
			{
				ImGui::Checkbox("z - Up", &zUp);
				ImGui::Checkbox("AABBs", &drawAABBs);
				ImGui::SliderFloat("Camera Speed [units/s]", &translation_speed, 0.0f,
				                   max_bvh_size);
				ImGui::SliderFloat("Rotation Speed [radiens/s]", &rotation_speed, 0.0f, M_PI);
				ImGui::ColorPicker3("Background", background, ImGuiColorEditFlags_NoSidePreview);
				ImGui::ColorPicker3("Wireframe", wireframeColor, ImGuiColorEditFlags_NoSidePreview);

				ImGui::EndTabItem();
			}

			if (ImGui::BeginTabItem("BVH"))
			{
				if (ImGui::TreeNode("Top level"))
				{
					ImGui::BeginListBox("##empty", ImVec2(-FLT_MIN, 0.0f));

					for (uint32_t i = 0; i < bvhs.size(); i++)
					{
						auto bvh = bvhs[i];
						if (!bvh->header.instance_count)
						{
							continue;
						}

						std::stringstream label;
						label << std::hex << bvh->va;
						if (ImGui::Selectable(label.str().c_str(), i == selectedTLAS))
						{
							selectedTLAS = i;
						}
					}

					ImGui::EndListBox();
					ImGui::TreePop();
				}
				ImGui::EndTabItem();
			}

			ImGui::EndTabBar();
		}
		ImGui::End();

		ImVec2 viewportPos;
		ImVec2 viewportSize;

		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("Viewport");
		{
			viewportPos = ImGui::GetCursorScreenPos();
			viewportSize = ImGui::GetWindowSize();
		}
		ImGui::End();
		ImGui::PopStyleVar();

		ImGui::End();

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glViewport((GLint)viewportPos.x, height - (GLint)viewportPos.y - (GLint)viewportSize.y,
		           (GLint)viewportSize.x, (GLint)viewportSize.y);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glEnable(GL_SCISSOR_TEST);
		glScissor((GLint)viewportPos.x, height - (GLint)viewportPos.y - (GLint)viewportSize.y,
		          (GLint)viewportSize.x, (GLint)viewportSize.y);

		DrawViewport(viewportSize.x / viewportSize.y);

		glDisable(GL_SCISSOR_TEST);
		glViewport(0, 0, width, height);

		glfwSwapBuffers(window);

		return !glfwWindowShouldClose(window);
	}
};

int main()
{
	Visualizer visualizer;

	if (!visualizer.Open())
	{
		return 1;
	}

	visualizer.LoadBVHs();

	while (visualizer.UpdateAndRender())
	{
	}

	visualizer.Close();
	return 0;
}
