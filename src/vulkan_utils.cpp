/*
 * Copyright © 2022 Konstantin Seurer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "vulkan_utils.h"

#include <cstring>
#include <iostream>
#include <vector>

static std::vector<const char*> layers = {
	// Validation is currently broken with ray tracing...
	// "VK_LAYER_KHRONOS_validation",
};

static std::vector<const char*> extensions = {
	"VK_KHR_acceleration_structure",
	"VK_KHR_deferred_host_operations",
};

static void CreateInstance(VulkanContext& context)
{
	VkApplicationInfo appInfo = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pNext = nullptr,
		.pApplicationName = "bvh-builder",
		.applicationVersion = VK_MAKE_VERSION(1, 0, 0),
		.pEngineName = "bvh-analyzer",
		.engineVersion = VK_MAKE_VERSION(1, 0, 0),
		.apiVersion = VK_API_VERSION_1_2,
	};

	VkInstanceCreateInfo info = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.pApplicationInfo = &appInfo,
		.enabledLayerCount = (uint32_t)layers.size(),
		.ppEnabledLayerNames = layers.data(),
		.enabledExtensionCount = 0,
		.ppEnabledExtensionNames = nullptr,
	};

	VK_ASSERT(vkCreateInstance(&info, nullptr, &context.instance));
}

static void SelectPhysicalDevice(VulkanContext& context)
{
	uint32_t physicalDeviceCount;
	VK_ASSERT(context.EnumeratePhysicalDevices(context.instance, &physicalDeviceCount, nullptr));

	std::vector<VkPhysicalDevice> devices(physicalDeviceCount);
	VK_ASSERT(
		context.EnumeratePhysicalDevices(context.instance, &physicalDeviceCount, devices.data()));

	for (auto pdevice : devices)
	{
		uint32_t proeprtyCount;
		VK_ASSERT(
			context.EnumerateDeviceExtensionProperties(pdevice, nullptr, &proeprtyCount, nullptr));

		std::vector<VkExtensionProperties> properties(proeprtyCount);
		VK_ASSERT(context.EnumerateDeviceExtensionProperties(pdevice, nullptr, &proeprtyCount,
		                                                     properties.data()));

		uint32_t supportCount = 0;
		for (const auto& property : properties)
		{
			for (auto extension : extensions)
			{
				if (strcmp(property.extensionName, extension) == 0)
				{
					supportCount++;
				}
			}
		}

		if (supportCount == extensions.size())
		{
			context.physicalDevice = pdevice;
		}
	}
}

void SelectQueueFamily(VulkanContext& context)
{
	uint32_t queueFamilyCount;
	context.GetPhysicalDeviceQueueFamilyProperties(context.physicalDevice, &queueFamilyCount,
	                                               nullptr);

	std::vector<VkQueueFamilyProperties> properties(queueFamilyCount);
	context.GetPhysicalDeviceQueueFamilyProperties(context.physicalDevice, &queueFamilyCount,
	                                               properties.data());

	for (uint32_t i = 0; i < queueFamilyCount; i++)
	{
		if ((properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) &&
		    (properties[i].queueFlags & VK_QUEUE_COMPUTE_BIT) &&
		    (properties[i].queueFlags & VK_QUEUE_TRANSFER_BIT))
		{
			context.queueFamily = i;
			return;
		}
	}

	abort();
}

static void CreateDevice(VulkanContext& context)
{
	VkPhysicalDeviceFeatures features = {};

	VkPhysicalDeviceBufferDeviceAddressFeatures bdaFeatures = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES,
		.pNext = nullptr,
		.bufferDeviceAddress = true,
	};

	VkPhysicalDeviceAccelerationStructureFeaturesKHR asFeatures = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR,
		.pNext = &bdaFeatures,
		.accelerationStructure = true,
		.accelerationStructureHostCommands = true,
	};

	VkPhysicalDeviceRayQueryFeaturesKHR rqFeatures = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR,
		.pNext = &asFeatures,
		.rayQuery = true,
	};

	float priority = 0.0;

	VkDeviceQueueCreateInfo queueInfo = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueFamilyIndex = context.queueFamily,
		.queueCount = 1,
		.pQueuePriorities = &priority,
	};

	VkDeviceCreateInfo info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pNext = &rqFeatures,
		.flags = 0,
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = &queueInfo,
		.enabledLayerCount = 0,
		.ppEnabledLayerNames = NULL,
		.enabledExtensionCount = (uint32_t)extensions.size(),
		.ppEnabledExtensionNames = extensions.data(),
		.pEnabledFeatures = &features,
	};

	VK_ASSERT(context.CreateDevice(context.physicalDevice, &info, nullptr, &context.device));
}

#define LOAD_INSTANCE_ENTRY_POINT(context, name)                                                   \
	context.name =                                                                                 \
		(PFN_vk##name)vkGetInstanceProcAddr(context.instance, (std::string("vk") + #name).c_str())

#define LOAD_DEVICE_ENTRY_POINT(context, name)                                                     \
	context.name =                                                                                 \
		(PFN_vk##name)vkGetDeviceProcAddr(context.device, (std::string("vk") + #name).c_str())

VulkanContext CreateContext()
{
	VulkanContext context;

	CreateInstance(context);

	LOAD_INSTANCE_ENTRY_POINT(context, EnumeratePhysicalDevices);
	LOAD_INSTANCE_ENTRY_POINT(context, EnumerateDeviceExtensionProperties);
	LOAD_INSTANCE_ENTRY_POINT(context, GetPhysicalDeviceQueueFamilyProperties);
	LOAD_INSTANCE_ENTRY_POINT(context, GetPhysicalDeviceMemoryProperties);
	LOAD_INSTANCE_ENTRY_POINT(context, CreateDevice);

	SelectPhysicalDevice(context);
	if (!context.physicalDevice)
	{
		std::cerr << "No suitable physical device found!" << std::endl;
		exit(1);
	}

	SelectQueueFamily(context);

	CreateDevice(context);

	LOAD_DEVICE_ENTRY_POINT(context, CreateBuffer);
	LOAD_DEVICE_ENTRY_POINT(context, GetBufferMemoryRequirements);
	LOAD_DEVICE_ENTRY_POINT(context, AllocateMemory);
	LOAD_DEVICE_ENTRY_POINT(context, BindBufferMemory);
	LOAD_DEVICE_ENTRY_POINT(context, DestroyBuffer);
	LOAD_DEVICE_ENTRY_POINT(context, FreeMemory);
	LOAD_DEVICE_ENTRY_POINT(context, GetBufferDeviceAddress);

	return context;
}

void DestroyContext(const VulkanContext& context)
{
	vkDestroyDevice(context.device, nullptr);
	vkDestroyInstance(context.instance, nullptr);
}

static uint32_t FindMemoryType(const VulkanContext& context,
                               const VkMemoryRequirements& requirements,
                               VkMemoryPropertyFlags flags)
{
	VkPhysicalDeviceMemoryProperties properties;
	context.GetPhysicalDeviceMemoryProperties(context.physicalDevice, &properties);

	for (uint32_t typeIndex = 0; typeIndex < properties.memoryTypeCount; typeIndex++)
	{
		if (requirements.memoryTypeBits & (1 << typeIndex))
		{
			const VkMemoryType& type = properties.memoryTypes[typeIndex];

			if ((type.propertyFlags & flags) == flags)
			{
				return typeIndex;
			}
		}
	}

	return 0;
}

VulkanBuffer CreateBuffer(const VulkanContext& context, uint32_t size)
{
	VulkanBuffer result;
	result.size = size;

	VkBufferCreateInfo bufferInfo = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.pNext = 0,
		.flags = 0,
		.size = size,
		.usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
	             VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR |
	             VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 1,
		.pQueueFamilyIndices = &context.queueFamily,
	};

	VK_ASSERT(context.CreateBuffer(context.device, &bufferInfo, nullptr, &result.buffer));

	VkMemoryRequirements requirements;
	context.GetBufferMemoryRequirements(context.device, result.buffer, &requirements);

	VkMemoryAllocateFlagsInfo allocateFlagsInfo = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO,
		.pNext = nullptr,
		.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT,
	};

	VkMemoryAllocateInfo memoryInfo = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.pNext = &allocateFlagsInfo,
		.allocationSize = requirements.size,
		.memoryTypeIndex = FindMemoryType(context, requirements,
	                                      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
	                                          VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
	};

	VK_ASSERT(context.AllocateMemory(context.device, &memoryInfo, nullptr, &result.memory));

	VK_ASSERT(context.BindBufferMemory(context.device, result.buffer, result.memory, 0));

	return result;
}

void DestroyBuffer(const VulkanContext& context, VulkanBuffer buffer)
{
	context.DestroyBuffer(context.device, buffer.buffer, nullptr);
	context.FreeMemory(context.device, buffer.memory, nullptr);
}

VkDeviceAddress GetBufferAddress(const VulkanContext& context, VkBuffer buffer)
{
	VkBufferDeviceAddressInfo info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
		.pNext = nullptr,
		.buffer = buffer,
	};

	return context.GetBufferDeviceAddress(context.device, &info);
}
