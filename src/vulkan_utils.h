/*
 * Copyright © 2022 Konstantin Seurer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef VULKAN_UTILS_H
#define VULKAN_UTILS_H

#include "../Vulkan-Headers/include/vulkan/vulkan.h"

#include <string>

std::string vk_to_string(VkResult result);

#define VK_ASSERT(expression)                                                                      \
	{                                                                                              \
		VkResult tempResult = expression;                                                          \
		if (tempResult != VK_SUCCESS)                                                              \
		{                                                                                          \
			std::cerr << #expression << " failed with " << (uint32_t)tempResult << std::endl;      \
			exit(tempResult);                                                                      \
		}                                                                                          \
	}

struct VulkanContext
{
	VkInstance instance = VK_NULL_HANDLE;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	uint32_t queueFamily = 0;
	VkDevice device = VK_NULL_HANDLE;

	PFN_vkEnumeratePhysicalDevices EnumeratePhysicalDevices;
	PFN_vkEnumerateDeviceExtensionProperties EnumerateDeviceExtensionProperties;
	PFN_vkGetPhysicalDeviceQueueFamilyProperties GetPhysicalDeviceQueueFamilyProperties;
	PFN_vkGetPhysicalDeviceMemoryProperties GetPhysicalDeviceMemoryProperties;
	PFN_vkCreateDevice CreateDevice;
	PFN_vkCreateBuffer CreateBuffer;
	PFN_vkGetBufferMemoryRequirements GetBufferMemoryRequirements;
	PFN_vkAllocateMemory AllocateMemory;
	PFN_vkBindBufferMemory BindBufferMemory;
	PFN_vkDestroyBuffer DestroyBuffer;
	PFN_vkFreeMemory FreeMemory;
	PFN_vkGetBufferDeviceAddress GetBufferDeviceAddress;
};

void SelectQueueFamily(VulkanContext& context);

VulkanContext CreateContext();
void DestroyContext(const VulkanContext& context);

struct VulkanBuffer
{
	uint32_t size;
	VkBuffer buffer;
	VkDeviceMemory memory;
};

VulkanBuffer CreateBuffer(const VulkanContext& context, uint32_t size);

void DestroyBuffer(const VulkanContext& context, VulkanBuffer buffer);

VkDeviceAddress GetBufferAddress(const VulkanContext& context, VkBuffer buffer);

#endif
