
#ifndef BVH_H
#define BVH_H

#include <memory>

#include "radv_acceleration_structure.h"

class Node
{
public:
	const radv_bvh_node_type type;

	union {
		radv_bvh_triangle_node triangle;
		radv_bvh_aabb_node aabb;
		radv_bvh_instance_node instance;
		radv_bvh_box32_node internal;
	};

public:
	Node(radv_bvh_node_type type) : type(type)
	{
	}

	virtual ~Node()
	{
	}
};

class InternalNode : public Node
{
public:
	std::shared_ptr<Node> children[4];

public:
	InternalNode() : Node(radv_bvh_node_internal)
	{
	}

	virtual ~InternalNode()
	{
	}
};

class BVH
{
public:
	std::shared_ptr<Node> root;
	radv_accel_struct_header header;
	uint64_t va;

public:
	BVH(const uint8_t* data, size_t size);
};

#endif
