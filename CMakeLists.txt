#
# Copyright © 2022 Konstantin Seurer
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#

cmake_minimum_required(VERSION 3.22)
project(bvh-analyzer)

set(CMAKE_CXX_STANDARD 20)

find_package(glfw3 3.3 REQUIRED)
find_package(OpenGL REQUIRED)
find_package(Vulkan REQUIRED)

add_executable(bvh-validator src/validator.cpp src/bvh.cpp)

add_executable(bvh-analyzer src/analyzer.cpp src/bvh.cpp)
target_include_directories(bvh-analyzer PRIVATE json/include)

set(IMGUI_SOURCE_FILEE
	imgui/backends/imgui_impl_glfw.cpp
	imgui/backends/imgui_impl_opengl3.cpp
	imgui/imgui.cpp
	imgui/imgui_draw.cpp
	imgui/imgui_tables.cpp
	imgui/imgui_widgets.cpp
)

add_executable(bvh-visualizer src/visualizer.cpp src/bvh.cpp ${IMGUI_SOURCE_FILEE})
target_include_directories(bvh-visualizer PRIVATE imgui glm)
target_link_libraries(bvh-visualizer PRIVATE glfw OpenGL::GL Vulkan::Vulkan)

add_executable(bvh-builder src/builder.cpp src/vulkan_utils.cpp src/tiny_gltf.cpp)
target_link_libraries(bvh-builder Vulkan::Vulkan)

add_library(VK_KSEURER_acceleration_structure_dump SHARED src/bvh_dump_layer.cpp src/vulkan_utils.cpp)

install(TARGETS VK_KSEURER_acceleration_structure_dump DESTINATION /usr/local/lib64)
install(FILES VK_KSEURER_acceleration_structure_dump.json DESTINATION /usr/local/share/vulkan/explicit_layer.d)
